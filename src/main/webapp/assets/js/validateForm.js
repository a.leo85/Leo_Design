function dictionary(){
    return {
        'patternMismatch' : 'Formato non valido',
        'rangeOverflow' : 'Il valore è più grande di %s',
        'rangeUnderflow' : 'Il valore è più piccolo di %s',
        'tooLong' : 'Non deve superare lunghezza %s caratteri',
        'tooShort' : 'Non deve essere più piccolo di %s caratteri',
        'stepMismatch' : 'Deve avere uno step di %s' ,
        'valueMissing' : 'Campo obbligatorio'
    };
}

var form = document.getElementById("signUpForm");
var form2 = document.getElementById("signinForm");
var form3 = document.getElementById("prodForm");
var form4 = document.getElementById("adminLogin");
var form5 = document.getElementById("searchForm");

if(form != null){
    validateForm(form);
}
if(form2 != null){
    validateForm(form2);
}
if(form3 != null){
    validateForm(form3);
}
if(form4 != null){
    validateForm(form4);
}
if(form5 != null){
    validateForm(form5);
}

function validateForm(form){
    const dict = dictionary();

    function _reportError(event){
        const _el = event.target;
        const errors = [];

        if(_el.validity.tooShort){
            errors.push(dict['tooShort'].replace("%s", _el.getAttribute("minlength")));
        }

        if(_el.validity.tooLong){
            errors.push(dict['tooLong'].replace("%s", _el.getAttribute("maxlength")));
        }

        if(_el.validity.patternMismatch){
            errors.push(dict['patternMismatch']);
        }

        if(_el.validity.valueMissing){
            errors.push(dict['valueMissing']);
        }

        if(_el.validity.rangeUnderflow){
            errors.push(dict['rangeUnderflow'].replace("%s", _el.getAttribute("min")));
        }

        if(_el.validity.rangeOverflow){
            errors.push(dict['rangeOverflow'].replace("%s", _el.getAttribute("max")));
        }

        if(_el.validity.stepMismatch){
            errors.push(dict['stepMismatch'].replace("%s", _el.getAttribute("step")));
        }

        _el.parentElement.nextElementSibling.textContent = errors.join('|');
    }

    function _reset(event){
        const _el = event.target;
        _el.parentElement.nextElementSibling.textContent = '';
    }

    form.setAttribute("novalidate", "true");
    const inputs = form.elements;
    for(let i=0; i < inputs.length; i++){
        let isValidInput = inputs[i].nodeName.match("INPUT|TEXTAREA|SELECT");
        if(inputs[i].willValidate !== 'undefined' && isValidInput){
            inputs[i].addEventListener('invalid',_reportError);
            inputs[i].addEventListener('focus',_reset);
        }
    }

    form.addEventListener('submit',function (event){
       if(form.checkValidity()){
           form.submit();
       } else {
           event.preventDefault();
       }
    });

}