<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">

<head>
  <jsp:include page="../partials/head.jsp">
    <jsp:param name="title" value="pagamento"/>
    <jsp:param name="styles" value="pagamento.css"/>
    <jsp:param name="script" value="sidebar.js,gifHover.js,alert.js"/>
  </jsp:include>
</head>

<body>

<header>

  <jsp:include page="../partials/header.jsp"/>

</header>

<div class="grid-x justify-content-center align-items-center">
  <c:if test="${not empty alert}">
    <%@include file="../partials/alert.jsp"%>
  </c:if>
</div>

<!-- Start pagamento -->
<form action="${context}/ordine/create" method="post">
<div class="pagamento display-flex align-items-center justify-content-around">

  <div class="input-data">
    <div>
      <h2>Email</h2>
      <input type="email" name="email" <c:if test="${not empty account.email}"> value="${account.email}" </c:if>>
    </div>
    <div>
      <h2>Indirizzo</h2>
      <input type="text" name="indirizzo" <c:if test="${not empty account.indirizzo}"> value="${account.indirizzo}" </c:if>>
    </div>

    <div>
      <h2>Provincia</h2>
      <input type="text" name="provincia" <c:if test="${not empty account.provincia}"> value="${account.provincia}" </c:if>>
    </div>

    <div>
      <h2>Cittá</h2>
      <input type="text" name="citta" <c:if test="${not empty account.citta}"> value="${account.citta}" </c:if>>
    </div>

    <div>
      <h2>CAP</h2>
      <input type="text" name="cap" <c:if test="${not empty account.cap}"> value="${account.cap}" </c:if>>
    </div>

    <div>
      <h2>Telefono</h2>
      <input type="tel" name="telefono" <c:if test="${not empty account.telefono}"> value="${account.telefono}" </c:if>>
    </div>
  </div>


    <div class="pay-method">
      <div class="input-data background-card" id="carta-di-credito">
        <h2>Carta di credito</h2>
        <input type="text" placeholder="XXXX-XXXX-XXXX-XXXX" name="carta">
      </div>


      <button class="button"><a href="${context}/carrello">Annulla</a></button>
      <button type="submit" class="button">Conferma</button>
    </div>
</div>
</form>
<!-- End pagamento -->

<!-- Start Footer -->

<jsp:include page="../partials/footer.jsp"/>

<!-- End Footer -->

</body>

</html>
