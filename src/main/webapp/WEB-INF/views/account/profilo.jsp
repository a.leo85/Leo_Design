<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../partials/head.jsp">
        <jsp:param name="title" value="Profilo"/>
        <jsp:param name="styles" value="crm.css,table.css"/>
        <jsp:param name="script" value="sidebar.js"/>
    </jsp:include>
</head>
<body>
<header>

    <div id="headerContainer" class="display-flex justify-content-between align-items-stretch">

        <!-- Start sidebar -->
        <div>
            <div class="menu-btn">
                <img src="${context}/assets/img/menu-start.png" class="toggle">
            </div>

            <div class="side-bar">
                <div  class="close-btn">
                    <img src="${context}/assets/img/menu-end.png" class="toggle">
                </div>
                <div class="menu">
                    <div class="item"><a href="${context}/accounts/profilo"><i ></i>I miei ordini</a> </div>
                    <div class="item"><a href="${context}/accounts/logout"><i ></i>Logout</a> </div>
                </div>
            </div>
        </div>

        <!-- End sidebar -->

    </div>
    <div class="dashboard justify-content-center align-items-center">
        <a href="${context}/accounts/profilo"><h1>I miei ordini</h1></a>
    </div>
</header>


<c:choose>
    <c:when test="${listaOrdini.size() > 0}">

        <!-- Start table -->

    <div class="container">
        <h1>I miei ordini</h1>
        <ul class="responsive-table">
            <li class="table-header">
                <div class="col-10"><h2>ID ordine</h2></div>
                <div class="col-15"><h2>Data</h2></div>
                <div class="col-10"><h2>Totale</h2></div>
                <div class="col-15"><h2>Stato</h2></div>
                <div class="col-20"><h2>Operazioni</h2></div>

            </li>
            <c:forEach items="${listaOrdini}" var="ordine">
                <li class="table-row">
                    <div class="col-10"><p>${ordine.IDordine}</p></div>
                    <div class="col-15"><p>${ordine.inserimento}</p></div>
                    <div class="col-10"><p>${ordine.totale}</p></div>
                    <div class="col-15"><p>${ordine.stato}</p></div>
                    <form action="${context}/ordine/delete-ordine" method="post">
                        <input type="hidden" name="idOrdine" value="${ordine.IDordine}">
                        <div class="col-20">
                            <input type="submit" value="Annulla ordine">
                        </div>
                    </form>
                </li>
            </c:forEach>

        </ul>
    </div>

        <!-- End table -->

        <!-- Start paginator -->
        <div class="pagination">
            <jsp:include page="../partials/paginator.jsp">
                <jsp:param name="resource" value="ordini"/>
            </jsp:include>
        </div>
        <!-- End paginator -->

        <!-- Start Footer -->

        <jsp:include page="../partials/footer.jsp"/>

        <!-- End Footer -->

    </c:when>
    <c:otherwise>
        <div class="grid-y justify-content-center align-items-center ">
            <h1>Non ci sono ordini</h1>
            <a href="${context}/index.html"><h2>Torna alla Home</h2></a>
        </div>

    </c:otherwise>
</c:choose>

</body>
</html>

