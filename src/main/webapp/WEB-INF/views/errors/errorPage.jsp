<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
  <jsp:include page="../partials/head.jsp">
    <jsp:param name="title" value="Error"/>
    <jsp:param name="styles" value="error.css"/>
    <jsp:param name="script" value="sidebar.js,gifHover.js"/>
  </jsp:include>

</head>
<body>

<header>

  <!-- Start Menu Bar -->

  <jsp:include page="../partials/header.jsp"/>

  <!-- End Menu Bar -->

</header>

  <div>
    <div class="errors grid-y display-flex justify-content-center align-items-center">
      <c:if test="${not empty message}">
        <h1> OPS qualcosa e' andato storto </h1>
      </c:if>
      <c:if test="${not empty alert}">
        <div class="notification ${alert.type}" id="alertContainer" style="width: 70%">
          <ol class="cell">
            <c:forEach var="msg" items="${alert.messages}">
              <li>${msg}</li>
            </c:forEach>
          </ol>
        </div>
      </c:if>
    </div>
  </div>

<!-- Start Footer -->

<jsp:include page="../partials/footer.jsp"/>

<!-- End Footer -->
</body>
</html>
