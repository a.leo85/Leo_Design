<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../partials/head.jsp">
        <jsp:param name="title" value="Login Admin"/>
        <jsp:param name="styles" value="product.css,login.css"/>
        <jsp:param name="script" value="sidebar.js,alert.js,validator.js"/>
    </jsp:include>
</head>
<body>

<div class="grid-x justify-content-center align-items-center">
    <c:if test="${not empty alert}">
        <%@include file="../partials/alert.jsp"%>
    </c:if>
</div>

    <!-- Start login -->

    <div class="login-admin" id="container">
        <div class="form-container-admin">
            <form action="${context}/accounts/secret" method="post" id="adminLogin">
                <h1>Accedi da Admin</h1>
                <p>o usa il tuo account</p>
                <input type="email" placeholder="Email" id="signinEmail" name="signinEmail" />
                <input type="password" placeholder="Password" id="signinPass" name="signinPass"/>
                <button type="submit">Accedi</button>
            </form>
        </div>
    </div>

    <!-- End login -->

    <jsp:include page="../partials/footer.jsp"/>
</body>
</html>
