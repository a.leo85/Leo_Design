<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../partials/head.jsp">
        <jsp:param name="title" value="Leo Design"/>
        <jsp:param name="styles" value="crm.css,table.css"/>
        <jsp:param name="script" value="sidebar.js"/>
    </jsp:include>
</head>
<body>
<header>

    <div id="headerContainer" class="display-flex justify-content-between align-items-stretch">

        <!-- Start sidebar -->
        <div>
            <div class="menu-btn">
                <img src="${context}/assets/img/menu-start.png" class="toggle">
            </div>

            <div class="side-bar">
                <div  class="close-btn">
                    <img src="${context}/assets/img/menu-end.png" class="toggle">
                </div>
                <div class="menu">
                    <div class="item"><a href="${context}/crm/accounts"><i ></i>Gestione Clienti</a></div>
                    <div class="item"><a href="${context}/crm/prodotti"><i ></i>Gestione Prodotti</a> </div>
                    <div class="item"><a href="${context}/crm/ordini"><i ></i>Gestione Ordini</a> </div>
                    <div class="item"><a href="${context}/accounts/logout"><i ></i>Logout</a> </div>
                </div>
            </div>
        </div>

        <!-- End sidebar -->

    </div>
    <div class="dashboard">
        <a href="${context}/crm/dashboard"><h1>Dashboard</h1></a>
    </div>
</header>

<!-- Start table -->

<div class="container">
    <h1>Gestione Prodotti</h1>
    <ul class="responsive-table">
        <li class="table-header">
            <div class="col-10"><h2>ID prodotto</h2></div>
            <div class="col-20"><h2>Nome</h2></div>
            <div class="col-30"><h2>Descrizione</h2></div>
            <div class="col-10"><h2>Prezzo</h2></div>
            <div class="col-10"><h2>Peso</h2></div>
            <div class="col-10"><h2>Disponibilita</h2></div>
            <div class="col-10"><h2>Operazioni</h2></div>

        </li>
        <c:forEach items="${listaProdotti}" var="prodotto">
            <li class="table-row">
                <div class="col-10"><p>${prodotto.idProdotto}</p></div>
                <div class="col-20"><p>${prodotto.nome}</p></div>
                <div class="col-30"><p>${prodotto.descrizione}</p></div>
                <div class="col-10"><p>€ ${prodotto.prezzo}</p></div>
                <div class="col-10"><p>${prodotto.peso}</p></div>
                <div class="col-10"><p>${prodotto.disponibilita}</p></div>
                <form action="${context}/crm/update-prodotto">
                    <input type="hidden" name="idProdotto" value="${prodotto.idProdotto}">
                    <div class="col-10"> <input type="submit" value="Modifica"> </div>
                </form>
            </li>
        </c:forEach>

    </ul>
</div>

<!-- End table -->

<form action="${context}/crm/aggiungi-prodotto" method="get">
    <div class="display-flex justify-content-center">
        <button type="submit" class="button" style="width: 50%; font-size: 100%; color: white"><h2>AGGIUNGI PRODOTTO</h2></button>
    </div>
</form>

<!-- Start paginator -->
<div class="pagination">
    <jsp:include page="../partials/paginator.jsp">
        <jsp:param name="resource" value="prodotti"/>
    </jsp:include>
</div>
<!-- End paginator -->

<!-- Start Footer -->

<jsp:include page="../partials/footer.jsp"/>

<!-- End Footer -->
</body>
</html>
