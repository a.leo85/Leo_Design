<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <jsp:include page="../partials/head.jsp">
        <jsp:param name="title" value="Leo Design"/>
        <jsp:param name="styles" value="crm.css"/>
        <jsp:param name="script" value="sidebar.js,alert.js"/>
    </jsp:include>
</head>
<body>
<header>

    <div id="headerContainer" class="display-flex justify-content-between align-items-stretch">

        <!-- Start sidebar -->
        <div>
            <div class="menu-btn">
                <img src="${context}/assets/img/menu-start.png" class="toggle">
            </div>

            <div class="side-bar">
                <div  class="close-btn">
                    <img src="${context}/assets/img/menu-end.png" class="toggle">
                </div>
                <div class="menu">
                    <div class="item"><a href="${context}/crm/accounts"><i ></i>Gestione Clienti</a></div>
                    <div class="item"><a href="${context}/crm/prodotti"><i ></i>Gestione Prodotti</a> </div>
                    <div class="item"><a href="${context}/crm/ordini"><i ></i>Gestione Ordini</a> </div>
                    <div class="item"><a href="${context}/accounts/logout"><i ></i>Logout</a> </div>
                </div>
            </div>
        </div>

        <!-- End sidebar -->

    </div>
    <div class="dashboard">
        <a href="${context}/crm/dashboard"><h1>Dashboard</h1></a>
    </div>
</header>

<div class="grid-x justify-content-center align-items-center">
    <c:if test="${not empty alert}">
        <%@include file="../partials/alert.jsp"%>
    </c:if>
</div>

<form action="${context}/crm/update-prodotto" method="post" class="display-flex justify-content-center align-items-center" id="prodForm">
    <input type="hidden" name="idProdotto" value="${prodotto.idProdotto}">
    <div class="aggiungi-prodotto grid-y justify-content-evenly align-items-center">
        <h1>Modifica Prodotto</h1>
        <div class=" display-flex justify-content-around align-items-center">
            <div class="insert-product-data">
                <div>
                    <label for="nome">
                        <h2>Nome</h2>
                    </label>
                    <input type="text" name="nome" id="nome" placeholder="nome" value="${prodotto.nome}">
                </div>
                <div>
                    <label for="descrizione">
                        <h2>Descrizione</h2>
                    </label>
                    <textarea name="descrizione" id="descrizione" >${prodotto.descrizione} </textarea>
                </div>
                <div>
                    <label for="prezzo">
                        <h2>Prezzo</h2>
                    </label>
                    <input type="text" name="prezzo" id="prezzo" placeholder="prezzo" value="${prodotto.prezzo}">
                </div>
                <div>
                    <label for="peso">
                        <h2>Peso</h2>
                    </label>
                    <input type="text" name="peso" id="peso" placeholder="peso" value="${prodotto.peso}">
                </div>
                <div>
                    <label for="disponibilita">
                        <h2>Disponibilita</h2>
                    </label>
                    <input type="text" name="disponibilita" id="disponibilita" placeholder="disponibilita" value="${prodotto.disponibilita}">
                </div>
            </div>

            <div class="insert-product-data">
                <div>
                    <label for="magazzino">
                        <h2>Magazzino</h2>
                    </label>
                    <select name="magazzino" id="magazzino">
                        <option value="1" <c:if test="${prodotto.magazzino.idMagazzino == 1}"> selected</c:if>>Scavolini</option>
                        <option value="2" <c:if test="${prodotto.magazzino.idMagazzino == 2}"> selected</c:if>>IKEA</option>
                        <option value="3" <c:if test="${prodotto.magazzino.idMagazzino == 3}"> selected</c:if>>PoltroneSofa</option>
                    </select>
                </div>
                <div>
                    <label for="categoria">
                        <h2>Categoria</h2>
                    </label>
                    <select name="categoria" id="categoria">
                        <option value="1" <c:if test="${prodotto.categoria.id == 1}"> selected</c:if>>Mobili, credenze, schedari</option>
                        <option value="2" <c:if test="${prodotto.categoria.id == 2}"> selected</c:if>>Armadi e guardaroba</option>
                        <option value="3" <c:if test="${prodotto.categoria.id == 3}"> selected</c:if>>Tavoli e scrivania</option>
                        <option value="4" <c:if test="${prodotto.categoria.id == 4}"> selected</c:if>>Sedie</option>
                        <option value="5" <c:if test="${prodotto.categoria.id =='5'}"> selected</c:if>>Divani</option>
                        <option value="6" <c:if test="${prodotto.categoria.id == '6'}"> selected</c:if>>Cassettiere</option>
                        <option value="7" <c:if test="${prodotto.categoria.id == '7'}"> selected</c:if>>Letti</option>
                        <option value="8" <c:if test="${prodotto.categoria.id == '8'}"> selected</c:if>>Mobili per bagno</option>
                        <option value="9" <c:if test="${prodotto.categoria.id == '9'}"> selected</c:if>>Mobili da cucina</option>
                        <option value="10" <c:if test="${prodotto.categoria.id == '10'}"> selected</c:if>>Ante frontali per cassetti da cucina</option>
                        <option value="11" <c:if test="${prodotto.categoria.id == '11'}"> selected</c:if>>Piani di lavoro per cucina</option>
                        <option value="12" <c:if test="${prodotto.categoria.id == '12'}"> selected</c:if>>Isola cucina</option>
                        <option value="13" <c:if test="${prodotto.categoria.id == '13'}"> selected</c:if>>Mensole cucina</option>
                        <option value="14" <c:if test="${prodotto.categoria.id == '14'}"> selected</c:if>>Illuminazione cucina</option>
                        <option value="15" <c:if test="${prodotto.categoria.id == '15'}"> selected</c:if>>Tavolini</option>
                        <option value="16" <c:if test="${prodotto.categoria.id == '16'}"> selected</c:if>>Pareti attrezzate</option>
                        <option value="17" <c:if test="${prodotto.categoria.id == '17'}"> selected</c:if>>Decorazioni</option>
                        <option value="18" <c:if test="${prodotto.categoria.id == '18'}"> selected</c:if>>Mobili da lavabo</option>
                        <option value="19" <c:if test="${prodotto.categoria.id == '19'}"> selected</c:if>>Accessori per bagno</option>
                        <option value="20" <c:if test="${prodotto.categoria.id == '20'}"> selected</c:if>>Specchi per bagno</option>
                        <option value="21" <c:if test="${prodotto.categoria.id == '21'}"> selected</c:if>>Lavabi bagno</option>
                        <option value="22" <c:if test="${prodotto.categoria.id == '22'}"> selected</c:if>>Docce</option>
                        <option value="23" <c:if test="${prodotto.categoria.id == '23'}"> selected</c:if>>Materassi</option>
                        <option value="24" <c:if test="${prodotto.categoria.id == '24'}"> selected</c:if>>Strutture letto</option>
                        <option value="25" <c:if test="${prodotto.categoria.id == '25'}"> selected</c:if>>Comodini</option>
                        <option value="26" <c:if test="${prodotto.categoria.id == '26'}"> selected</c:if>>Basi e reti a doghe</option>
                    </select>
                </div>
                <div>
                    <label for="immagine1">
                        <h2>Immagine 1</h2>
                    </label>
                    <input type="text" name="immagine1" id="immagine1" placeholder="immagine.jpg" value="${prodotto.immagine1}">
                </div>
                <div>
                    <label for="immagine2">
                        <h2>Immagine 2</h2>
                    </label>
                    <input type="text" name="immagine2" id="immagine2" placeholder="immagine.jpg" value="${prodotto.immagine2}">
                </div>
                <div>
                    <label for="immagine3">
                        <h2>Immagine 3</h2>
                    </label>
                    <input type="text" name="immagine3" id="immagine3" placeholder="immagine.jpg" value="${prodotto.immagine3}">
                </div>
                <div>
                    <button class="button" style="width: 15vh; font-size: 2vh; color: white"><a href="${context}/crm/prodotti">Annulla</a></button>
                    <button type="submit" class="button" style="width: 15vh; font-size: 2vh; color: white">modifica</button>
                </div>

            </div>
        </div>
    </div>


</form>

</body>
</html>
