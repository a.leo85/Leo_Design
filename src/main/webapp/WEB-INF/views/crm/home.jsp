<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../partials/head.jsp">
        <jsp:param name="title" value="Leo Design"/>
        <jsp:param name="styles" value="crm.css,card.css"/>
        <jsp:param name="script" value="sidebar.js"/>
    </jsp:include>
</head>
<body>
<header>

    <div id="headerContainer" class="display-flex justify-content-between align-items-stretch">

        <!-- Start sidebar -->
        <div>
            <div class="menu-btn">
                <img src="${context}/assets/img/menu-start.png" class="toggle">
            </div>

            <div class="side-bar">
                <div  class="close-btn">
                    <img src="${context}/assets/img/menu-end.png" class="toggle">
                </div>
                <div class="menu">
                    <div class="item"><a href="${context}/crm/accounts"><i ></i>Gestione Clienti</a></div>
                    <div class="item"><a href="${context}/crm/prodotti"><i ></i>Gestione Prodotti</a> </div>
                    <div class="item"><a href="${context}/crm/ordini"><i ></i>Gestione Ordini</a> </div>
                    <div class="item"><a href="${context}/accounts/logout"><i ></i>Logout</a> </div>
                </div>
            </div>
        </div>

        <!-- End sidebar -->

    </div>
    <div class="dashboard">
        <a href="${context}/crm/dashboard"><h1>Dashboard</h1></a>
    </div>
</header>

<div class="cards display-flex justify-content-center align-items-center">
        <div class="justify-content-between display-inline-flex">
            <div class="card">
                <a href="${context}/crm/accounts">
                    <div class="card-img">
                        <img src="${context}/assets/img/gestione-account.gif" alt="gestione account">

                    </div>
                    <div class="card-info">
                        <div class="card-text">
                            <h1 class="text-title">Gestione clienti</h1>
                        </div>
                    </div>
                </a>
            </div>
        </div>

    <div class="justify-content-between display-inline-flex">
        <div class="card">
            <a href="${context}/crm/prodotti">
                <div class="card-img">
                    <img src="${context}/assets/img/gestione-prodotti.gif" alt="gestione prodotti">

                </div>
                <div class="card-info">
                    <div class="card-text">
                        <h1 class="text-title">Gestione prodotti</h1>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="justify-content-between display-inline-flex">
        <div class="card">
            <a href="${context}/crm/ordini">
                <div class="card-img">
                    <img src="${context}/assets/img/gestione-ordini.gif" alt="gestione ordini">

                </div>
                <div class="card-info">
                    <div class="card-text">
                        <h1 class="text-title">Gestione ordini</h1>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<!-- Start Footer -->

<jsp:include page="../partials/footer.jsp"/>

<!-- End Footer -->
</body>
</html>
