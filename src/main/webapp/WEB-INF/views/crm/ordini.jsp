<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../partials/head.jsp">
        <jsp:param name="title" value="Leo Design"/>
        <jsp:param name="styles" value="crm.css,table.css"/>
        <jsp:param name="script" value="sidebar.js"/>
    </jsp:include>
</head>
<body>
<header>

    <div id="headerContainer" class="display-flex justify-content-between align-items-stretch">

        <!-- Start sidebar -->
        <div>
            <div class="menu-btn">
                <img src="${context}/assets/img/menu-start.png" class="toggle">
            </div>

            <div class="side-bar">
                <div  class="close-btn">
                    <img src="${context}/assets/img/menu-end.png" class="toggle">
                </div>
                <div class="menu">
                    <div class="item"><a href="${context}/crm/accounts"><i ></i>Gestione Clienti</a></div>
                    <div class="item"><a href="${context}/crm/prodotti"><i ></i>Gestione Prodotti</a> </div>
                    <div class="item"><a href="${context}/crm/ordini"><i ></i>Gestione Ordini</a> </div>
                    <div class="item"><a href="${context}/accounts/logout"><i ></i>Logout</a> </div>
                </div>
            </div>
        </div>

        <!-- End sidebar -->

    </div>
    <div class="dashboard">
        <a href="${context}/crm/dashboard"><h1>Dashboard</h1></a>
    </div>
</header>

<!-- Start table -->

<div class="container">
    <h1>Gestione Ordini</h1>
    <ul class="responsive-table">
        <li class="table-header">
            <div class="col-10"><h2>ID ordine</h2></div>
            <div class="col-15"><h2>Data</h2></div>
            <div class="col-10"><h2>Totale</h2></div>
            <div class="col-15"><h2>Stato</h2></div>
            <div class="col-20"><h2>Operazioni</h2></div>

        </li>
        <c:forEach items="${listaOrdini}" var="ordine">
            <li class="table-row">
                <div class="col-10"><p>${ordine.IDordine}</p></div>
                <div class="col-15"><p>${ordine.inserimento}</p></div>
                <div class="col-10"><p>${ordine.totale}</p></div>
                <div class="col-15"><p>${ordine.stato}</p></div>
                <form action="${context}/crm/update-ordine" method="post">
                    <input type="hidden" name="idOrdine" value="${ordine.IDordine}">
                    <div class="col-20">
                        <select name="stato">
                            <option value="IN ELABORAZIONE" <c:if test="${ordine.stato  == 'IN ELABORAZIONE'}"> selected </c:if> >IN ELABORAZIONE</option>
                            <option value="IN TRANSITO" <c:if test="${ordine.stato  == 'IN TRANSITO'}"> selected </c:if> >IN TRANSITO</option>
                            <option value="SPEDITO" <c:if test="${ordine.stato  == 'SPEDITO'}"> selected </c:if> >SPEDITO</option>
                            <option value="IN CONSEGNA" <c:if test="${ordine.stato  == 'IN CONSEGNA'}"> selected </c:if> >IN CONSEGNA</option>
                            <option value="CONSEGNATO" <c:if test="${ordine.stato  == 'CONSEGNATO'}"> selected </c:if> >CONSEGNATO</option>
                        </select>
                        <input type="submit" value="AGGIORNA">
                    </div>
                </form>
                <form action="${context}/crm/delete-ordine" method="post">
                    <input type="hidden" name="idOrdine" value="${ordine.IDordine}">
                    <div class="col-20">
                        <input type="submit" value="ELIMINA">
                    </div>
                </form>
            </li>
        </c:forEach>

    </ul>
</div>

<!-- End table -->

<!-- Start paginator -->
<div class="pagination">
    <jsp:include page="../partials/paginator.jsp">
        <jsp:param name="resource" value="ordini"/>
    </jsp:include>
</div>
<!-- End paginator -->

<!-- Start Footer -->

<jsp:include page="../partials/footer.jsp"/>

<!-- End Footer -->
</body>
</html>

