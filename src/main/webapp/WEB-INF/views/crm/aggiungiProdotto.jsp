<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <jsp:include page="../partials/head.jsp">
        <jsp:param name="title" value="Leo Design"/>
        <jsp:param name="styles" value="crm.css"/>
        <jsp:param name="script" value="sidebar.js,alert.js"/>
    </jsp:include>
</head>
<body>
<header>

    <div id="headerContainer" class="display-flex justify-content-between align-items-stretch">

        <!-- Start sidebar -->
        <div>
            <div class="menu-btn">
                <img src="${context}/assets/img/menu-start.png" class="toggle">
            </div>

            <div class="side-bar">
                <div  class="close-btn">
                    <img src="${context}/assets/img/menu-end.png" class="toggle">
                </div>
                <div class="menu">
                    <div class="item"><a href="${context}/crm/accounts"><i ></i>Gestione Clienti</a></div>
                    <div class="item"><a href="${context}/crm/prodotti"><i ></i>Gestione Prodotti</a> </div>
                    <div class="item"><a href="${context}/crm/ordini"><i ></i>Gestione Ordini</a> </div>
                    <div class="item"><a href="${context}/accounts/logout"><i ></i>Logout</a> </div>
                </div>
            </div>
        </div>

        <!-- End sidebar -->

    </div>
    <div class="dashboard">
        <a href="${context}/crm/dashboard"><h1>Dashboard</h1></a>
    </div>
</header>
<div class="grid-x justify-content-center align-items-center">
    <c:if test="${not empty alert}">
        <%@include file="../partials/alert.jsp"%>
    </c:if>
</div>
<form action="${context}/crm/aggiungi-prodotto" method="post" class="display-flex justify-content-center align-items-center" id="prodForm">
    <div class="aggiungi-prodotto grid-y justify-content-evenly align-items-center">
        <h1>Aggiungi Prodotto</h1>
        <div class=" display-flex justify-content-around align-items-center">
            <div class="insert-product-data">
                <div>
                    <label for="nome">
                        <h2>Nome</h2>
                    </label>
                    <input type="text" name="nome" id="nome" placeholder="nome">
                </div>
                <div>
                    <label for="descrizione">
                        <h2>Descrizione</h2>
                    </label>
                    <textarea name="descrizione" id="descrizione"> </textarea>
                </div>
                <div>
                    <label for="prezzo">
                        <h2>Prezzo</h2>
                    </label>
                    <input type="text" name="prezzo" id="prezzo" placeholder="prezzo">
                </div>
                <div>
                    <label for="peso">
                        <h2>Peso</h2>
                    </label>
                    <input type="text" name="peso" id="peso" placeholder="peso">
                </div>
                <div>
                    <label for="disponibilita">
                        <h2>Disponibilita</h2>
                    </label>
                    <input type="text" name="disponibilita" id="disponibilita" placeholder="disponibilita">
                </div>
            </div>

            <div class="insert-product-data">
                <div>
                    <label for="magazzino">
                        <h2>Magazzino</h2>
                    </label>
                    <select name="magazzino" id="magazzino">
                        <option value="1">Scavolini</option>
                        <option value="2">IKEA</option>
                        <option value="3">PoltroneSofa</option>
                    </select>
                </div>
                <div>
                    <label for="categoria">
                        <h2>Categoria</h2>
                    </label>
                    <select name="categoria" id="categoria">
                        <option value="1">Mobili, credenze, schedari</option>
                        <option value="2">Armadi e guardaroba</option>
                        <option value="3">Tavoli e scrivania</option>
                        <option value="4">Sedie</option>
                        <option value="5">Divani</option>
                        <option value="6">Cassettiere</option>
                        <option value="7">Letti</option>
                        <option value="8">Mobili per bagno</option>
                        <option value="9">Mobili da cucina</option>
                        <option value="10">Ante frontali per cassetti da cucina</option>
                        <option value="11">Piani di lavoro per cucina</option>
                        <option value="12">Isola cucina</option>
                        <option value="13">Mensole cucina</option>
                        <option value="14">Illuminazione cucina</option>
                        <option value="15">Tavolini</option>
                        <option value="16">Pareti attrezzate</option>
                        <option value="17">Decorazioni</option>
                        <option value="18">Mobili da lavabo</option>
                        <option value="19">Accessori per bagno</option>
                        <option value="20">Specchi per bagno</option>
                        <option value="21">Lavabi bagno</option>
                        <option value="22">Docce</option>
                        <option value="23">Materassi</option>
                        <option value="24">Strutture letto</option>
                        <option value="25">Comodini</option>
                        <option value="26">Basi e reti a doghe</option>
                    </select>
                </div>
                <div>
                    <label for="immagine1">
                        <h2>Immagine 1</h2>
                    </label>
                    <input type="text" name="immagine1" id="immagine1" placeholder="immagine.jpg">
                </div>
                <div>
                    <label for="immagine2">
                        <h2>Immagine 2</h2>
                    </label>
                    <input type="text" name="immagine2" id="immagine2" placeholder="immagine.jpg">
                </div>
                <div>
                    <label for="immagine3">
                        <h2>Immagine 3</h2>
                    </label>
                    <input type="text" name="immagine3" id="immagine3" placeholder="immagine.jpg">
                </div>
                <div>
                    <button class="button" style="width: 15vh; font-size: 2vh; color: white"><a href="${context}/crm/prodotti">Annulla</a></button>
                    <button type="submit" class="button" style="width: 15vh; font-size: 2vh; color: white" >aggiungi</button>
                </div>

            </div>
        </div>
    </div>


</form>

</body>
</html>
