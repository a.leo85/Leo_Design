<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../partials/head.jsp">
        <jsp:param name="title" value="Leo Design"/>
        <jsp:param name="styles" value="crm.css,table.css"/>
        <jsp:param name="script" value="sidebar.js"/>
    </jsp:include>
</head>
<body>
<header>

    <div id="headerContainer" class="display-flex justify-content-between align-items-stretch">

        <!-- Start sidebar -->
        <div>
            <div class="menu-btn">
                <img src="${context}/assets/img/menu-start.png" class="toggle">
            </div>

            <div class="side-bar">
                <div  class="close-btn">
                    <img src="${context}/assets/img/menu-end.png" class="toggle">
                </div>
                <div class="menu">
                    <div class="item"><a href="${context}/crm/accounts"><i ></i>Gestione Clienti</a></div>
                    <div class="item"><a href="${context}/crm/prodotti"><i ></i>Gestione Prodotti</a> </div>
                    <div class="item"><a href="${context}/crm/ordini"><i ></i>Gestione Ordini</a> </div>
                    <div class="item"><a href="${context}/accounts/logout"><i ></i>Logout</a> </div>
                </div>
            </div>
        </div>

        <!-- End sidebar -->

    </div>
    <div class="dashboard">
        <a href="${context}/crm/dashboard"><h1>Dashboard</h1></a>
    </div>
</header>

<!-- Start table -->

<div class="container">
    <h1>Gestione Clienti</h1>
    <ul class="responsive-table">
        <li class="table-header">
            <div class="col-20"><h2>Email</h2></div>
            <div class="col-10"><h2>Password</h2></div>
            <div class="col-10"><h2>Nome</h2></div>
            <div class="col-10"><h2>Cognome</h2></div>
            <div class="col-10"><h2>Telefono</h2></div>
            <div class="col-10"><h2>Indirizzo</h2></div>
            <div class="col-5"><h2>Provincia</h2></div>
            <div class="col-5"><h2>Citta</h2></div>
            <div class="col-5"><h2>CAP</h2></div>
            <div class="col-10"><h2>Carta</h2></div>
            <div class="col-5"><h2>Operazioni</h2></div>
        </li>
        <c:forEach items="${listaAccount}" var="account">
            <li class="table-row">
                <div class="col-20"><p>${account.email}</p></div>
                <div class="col-10"><p>${account.password}</p></div>
                <div class="col-10"><p>${account.nome}</p></div>
                <div class="col-10"><p>${account.cognome}</p></div>
                <div class="col-10"><p>${account.telefono}</p></div>
                <div class="col-10"><p>${account.indirizzo}</p></div>
                <div class="col-5"><p>${account.provincia}</p></div>
                <div class="col-5"><p>${account.citta}</p></div>
                <div class="col-5"><p>${account.cap}</p></div>
                <div class="col-10"><p>${account.carta}</p></div>
                <form action="${context}/crm/remove-account" method="post">
                    <input type="hidden" name="email" value="${account.email}">
                <div class="col col-5"> <c:if test="${not account.admin}"><input type="submit" value="Elimina"></c:if> </div>
                </form>
            </li>
        </c:forEach>

    </ul>
</div>

<!-- End table -->

<!-- Start paginator -->
<div class="pagination">
    <jsp:include page="../partials/paginator.jsp">
        <jsp:param name="resource" value="accounts"/>
    </jsp:include>
</div>
<!-- End paginator -->


<!-- Start Footer -->

<jsp:include page="../partials/footer.jsp"/>

<!-- End Footer -->
</body>
</html>
