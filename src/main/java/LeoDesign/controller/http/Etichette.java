package LeoDesign.controller.http;

public interface Etichette {
    public static final String LISTA_ACCOUNT = "listaAccount";
    public static final String ACCOUNT = "account";
    public static final String LISTA_PRODOTTI = "listaProdotti";
    public static final String PRODOTTO = "prodotto";
    public static final String LSITA_ORDINI = "listaOrdini";
    public static final String NOME_CATEGORIA = "categoria";
    public static final String CARRELLO = "carrello";
}
