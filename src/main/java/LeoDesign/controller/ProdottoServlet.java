package LeoDesign.controller;

import LeoDesign.controller.http.Controller;
import LeoDesign.model.components.Paginator;
import LeoDesign.model.prodotto.Prodotto;
import LeoDesign.model.prodotto.SqlProdottoDao;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "ProdottoServlet", value = "/prodotti/*")
public class ProdottoServlet extends Controller {
    SqlProdottoDao serviceProduct;

    @Override
    public void init() throws ServletException {
        super.init();
        serviceProduct = new SqlProdottoDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path;
        if(request.getPathInfo() != null) path = request.getPathInfo();
        else path = "/";
        switch (path) {
            case "/show":
                int page = 0;
                try {
                    page = parsePage(request);
                }catch (NumberFormatException e) {
                    page = 1;
                }

                Paginator paginator = new Paginator(page, 5);
                int size = 0;
                try {
                    size = serviceProduct.countAll();
                    List<Prodotto> prodotti = serviceProduct.fetchProdotti(paginator);
                    request.setAttribute("pages", paginator.getPages(size));
                    request.setAttribute(LISTA_PRODOTTI, prodotti);
                    request.getRequestDispatcher(view("site/shop")).forward(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }

                break;
            case "/show-prodotto": {
                try {
                    int idProdotto = Integer.parseInt(request.getParameter("idProdotto"));
                    Optional<Prodotto> prodotto = serviceProduct.fetchProdotto(idProdotto);
                    if (prodotto.isPresent()) {
                        request.setAttribute(PRODOTTO, prodotto.get());
                        String fullUrl = request.getRequestURI() + "?" + request.getQueryString();
                        request.setAttribute("fullUrl", fullUrl);
                        request.getRequestDispatcher(view("site/shop-single")).forward(request, response);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
            }
            break;
        }
    }

}
