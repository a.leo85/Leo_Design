package LeoDesign.controller;

import LeoDesign.controller.http.Controller;
import LeoDesign.model.categoria.Categoria;
import LeoDesign.model.categoria.SqlCategoriaDao;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "CategoriaServlet", value = "/categoria/*")
public class CategoriaServlet extends Controller{
    SqlCategoriaDao serviceCategory;
    @Override
    public void init() throws ServletException {
        super.init();
        serviceCategory = new SqlCategoriaDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = getPath(request);
        Categoria categoria = null;

        switch (path) {
            case "/Mobili-credenze-schedari":

                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(1);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Armadi-e-guardaroba":

                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(2);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Tavoli-e-scrivania":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(3);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Sedie":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(4);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Divani":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(5);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Cassettiere":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(6);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Letti":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(7);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Mobili-per-bagno":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(8);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Mobili-da-cucina":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(9);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Ante-frontali-per-cassetti-da-cucina":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(10);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Piani-di-lavoro-per-cucina":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(11);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Isola-cucina":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(12);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;



            case "/Mensole-cucina":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(13);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Illuminazione-cucina":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(14);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Tavolini":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(15);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Pareti-attrezzate":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(16);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Decorazioni":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(17);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Mobili-da-lavabo":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(18);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Accessori-per-bagno":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(19);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Specchi-per-bagno":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(20);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Lavabi-bagno":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(21);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Docce":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(22);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            case "/Materassi":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(23);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Strutture-letto":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(24);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Comodini":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(25);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;

            case "/Basi-e-reti-a-doghe":
                try {
                    categoria = serviceCategory.fetchCategoriaWithProdotti(26);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
                request.setAttribute(LISTA_PRODOTTI, categoria.getProdotti());
                request.setAttribute(NOME_CATEGORIA, categoria.getTitolo());
                request.getRequestDispatcher(view("site/shop")).forward(request, response);
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Prodotto non trovato");
        }
    }

}
