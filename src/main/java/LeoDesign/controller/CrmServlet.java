package LeoDesign.controller;

import LeoDesign.controller.http.Controller;
import LeoDesign.controller.http.InvalidRequestException;
import LeoDesign.model.account.Account;
import LeoDesign.model.account.SqlAccountDao;
import LeoDesign.model.categoria.Categoria;
import LeoDesign.model.categoria.SqlCategoriaDao;
import LeoDesign.model.components.Alert;
import LeoDesign.model.components.Paginator;
import LeoDesign.model.magazzino.Magazzino;
import LeoDesign.model.magazzino.SqlMagazzinoDao;
import LeoDesign.model.ordine.Ordine;
import LeoDesign.model.ordine.SqlOrdineDao;
import LeoDesign.model.prodotto.Prodotto;
import LeoDesign.model.prodotto.ProductValidator;
import LeoDesign.model.prodotto.SqlProdottoDao;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "CrmServlet", value = "/crm/*")
public class CrmServlet extends Controller {
    private SqlAccountDao serviceAccount;
    private SqlProdottoDao serviceProduct;
    private SqlOrdineDao serviceOrder;
    private SqlMagazzinoDao serviceMagazzino;
    private SqlCategoriaDao serviceCategoria;

    @Override
    public void init() throws ServletException {
        super.init();
        serviceAccount = new SqlAccountDao();
        serviceProduct = new SqlProdottoDao();
        serviceOrder = new SqlOrdineDao();
        serviceMagazzino = new SqlMagazzinoDao();
        serviceCategoria = new SqlCategoriaDao();

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = getPath(request);
        switch (path){
            case "/dashboard":
                request.getRequestDispatcher(view("crm/home")).forward(request,response);
                break;
            case "/accounts": {

                int page;
                try {
                    page = parsePage(request);
                }catch (NumberFormatException e) {
                    page = 1;
                }

                Paginator paginator = new Paginator(page, 5);
                int size = 0;
                try {
                    size = serviceAccount.countAll();
                    List<Account> accounts = serviceAccount.fetchAccounts(paginator);
                    request.setAttribute("pages", paginator.getPages(size));
                    request.setAttribute(LISTA_ACCOUNT, accounts);
                    request.getRequestDispatcher(view("crm/accounts")).forward(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }

            }
                break;
            case "/prodotti": {
                int page = 0;
                try {
                    page = parsePage(request);
                }catch (NumberFormatException e) {
                    page = 1;
                }

                Paginator paginator = new Paginator(page, 5);
                int size = 0;
                try {
                    size = serviceProduct.countAll();
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }

                try {
                    List<Prodotto> prodotti = serviceProduct.fetchProdotti(paginator);
                    request.setAttribute("pages", paginator.getPages(size));
                    request.setAttribute(LISTA_PRODOTTI, prodotti);
                    request.getRequestDispatcher(view("crm/prodotti")).forward(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
            }
                break;

            case "/aggiungi-prodotto": {
                request.getRequestDispatcher(view("crm/aggiungiProdotto")).forward(request, response);
            }
            break;
            case "/update-prodotto": {
                int idProdotto = Integer.parseInt(request.getParameter("idProdotto"));
                try {
                    Optional<Prodotto> prodotto = serviceProduct.fetchProdotto(idProdotto);
                    request.setAttribute(PRODOTTO, prodotto.get());
                    request.getRequestDispatcher(view("crm/modificaProdotto")).forward(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
            }
            break;

            case "/ordini": {
                int page = 0;
                try {
                    page = parsePage(request);
                }catch (NumberFormatException e) {
                    page = 1;
                }

                Paginator paginator = new Paginator(page, 5);
                int size = 0;
                try {
                    size = serviceOrder.countAll();
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }

                try {
                    List<Ordine> ordini = serviceOrder.fetchOrdini(paginator);
                    request.setAttribute("pages", paginator.getPages(size));
                    request.setAttribute(LSITA_ORDINI, ordini);
                    request.getRequestDispatcher(view("crm/ordini")).forward(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND,"Risorsa non trovata");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = getPath(request);
        switch (path){
            case "/remove-account": {
                try {
                    String email = request.getParameter("email");
                    serviceAccount.deleteAccount(email);
                    response.sendRedirect("../crm/accounts");
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                }
            }
                break;
            case "/aggiungi-prodotto": {
                try {
                        authorize(request.getSession(false));
                        request.setAttribute("back", view("crm/aggiungiProdotto"));
                        validate(ProductValidator.validateForm(request));
                        String nome, descrizione, immagine1,immagine2,immagine3;
                        int disponibilita, idMagazzino, idCategoria;
                        float prezzo, peso;
                        nome = request.getParameter("nome");
                        descrizione = request.getParameter("descrizione");
                        immagine1 = "assets/img/" + request.getParameter("immagine1");
                        immagine2 = "assets/img/" + request.getParameter("immagine2");
                        immagine3 = "assets/img/" + request.getParameter("immagine3");
                        disponibilita = Integer.parseInt(request.getParameter("disponibilita"));
                        idMagazzino = Integer.parseInt(request.getParameter("magazzino"));
                        idCategoria = Integer.parseInt(request.getParameter("categoria"));
                        prezzo = Float.parseFloat(request.getParameter("prezzo"));
                        peso = Float.parseFloat(request.getParameter("peso"));
                        Prodotto prodotto = new Prodotto();
                        prodotto.setNome(nome);
                        prodotto.setDescrizione(descrizione);
                        prodotto.setImmagine1(immagine1);
                        prodotto.setImmagine2(immagine2);
                        prodotto.setImmagine3(immagine3);
                        prodotto.setDisponibilita(disponibilita);
                        Magazzino magazzino = serviceMagazzino.fetchMagazzino(idMagazzino);
                        prodotto.setMagazzino(magazzino);
                        Categoria categoria = serviceCategoria.fetchCategoriaById(idCategoria);
                        prodotto.setCategoria(categoria);
                        prodotto.setPrezzo(prezzo);
                        prodotto.setPeso(peso);
                        serviceProduct.createProdotto(prodotto);
                        response.sendRedirect("../crm/prodotti");

                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                } catch (InvalidRequestException e) {
                    log(e.getMessage());
                    e.handle(request, response);
                }

            }
            break;
            case "/update-prodotto": {
                try {
                    authorize(request.getSession(false));
                    request.setAttribute("back", view("crm/modificaProdotto"));
                    validate(ProductValidator.validateForm(request));
                    String nome, descrizione, immagine1,immagine2,immagine3;
                    int idProdotto, disponibilita, idMagazzino, idCategoria;
                    float prezzo, peso;
                    idProdotto = Integer.parseInt(request.getParameter("idProdotto"));
                    nome = request.getParameter("nome");
                    descrizione = request.getParameter("descrizione");
                    immagine1 = request.getParameter("immagine1");
                    immagine2 = request.getParameter("immagine2");
                    immagine3 = request.getParameter("immagine3");
                    disponibilita = Integer.parseInt(request.getParameter("disponibilita"));
                    idMagazzino = Integer.parseInt(request.getParameter("magazzino"));
                    idCategoria = Integer.parseInt(request.getParameter("categoria"));
                    prezzo = Float.parseFloat(request.getParameter("prezzo"));
                    peso = Float.parseFloat(request.getParameter("peso"));

                    Prodotto prodotto = new Prodotto();
                    prodotto.setIdProdotto(idProdotto);
                    prodotto.setNome(nome);
                    prodotto.setDescrizione(descrizione);
                    prodotto.setImmagine1(immagine1);
                    prodotto.setImmagine2(immagine2);
                    prodotto.setImmagine3(immagine3);
                    prodotto.setDisponibilita(disponibilita);
                    Magazzino magazzino = serviceMagazzino.fetchMagazzino(idMagazzino);
                    prodotto.setMagazzino(magazzino);
                    Categoria categoria = serviceCategoria.fetchCategoriaById(idCategoria);
                    prodotto.setCategoria(categoria);
                    prodotto.setPrezzo(prezzo);
                    prodotto.setPeso(peso);
                    serviceProduct.updateProdotto(prodotto);
                    response.sendRedirect("../crm/prodotti");

                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                } catch (InvalidRequestException e) {
                    log(e.getMessage());
                    e.handle(request, response);
                }
            }
            break;
            case "/update-ordine": {
                try {
                    int id = Integer.parseInt(request.getParameter("idOrdine"));
                    String stato = request.getParameter("stato");
                    serviceOrder.updateOrdine(stato, id);
                    response.sendRedirect("../crm/ordini");
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            break;
            case "/delete-ordine": {
                try {
                    int id = Integer.parseInt(request.getParameter("idOrdine"));
                    if(serviceOrder.deleteOrdine(id)) {
                        List<String> msg = new ArrayList<>();
                        msg.add("Ordine annullato");
                        request.setAttribute("alert", new Alert(msg, "success"));
                    }
                    response.sendRedirect("../crm/ordini");
                } catch (SQLException e) {
                    e.printStackTrace();
                    log(e.getMessage());
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND,"Risorsa non trovata");
        }
    }
}
