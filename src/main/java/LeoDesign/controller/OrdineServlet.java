package LeoDesign.controller;

import LeoDesign.controller.http.Controller;
import LeoDesign.controller.http.InvalidRequestException;
import LeoDesign.model.account.*;
import LeoDesign.model.carrello.Carrello;
import LeoDesign.model.carrello.SqlCarrelloDao;
import LeoDesign.model.components.Alert;
import LeoDesign.model.components.Paginator;
import LeoDesign.model.ordine.OrderValidator;
import LeoDesign.model.ordine.Ordine;
import LeoDesign.model.ordine.SqlOrdineDao;
import LeoDesign.model.ordine.createOrderDao;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "OrdineServlet", value = "/ordine/*")
public class OrdineServlet extends Controller {
    private SqlOrdineDao serviceOrder;
    private SqlAccountDao serviceAccount;
    private SqlCarrelloDao serviceCart;
    private createOrderDao serviceCreate;

    @Override
    public void init() throws ServletException {
        super.init();
        serviceOrder = new SqlOrdineDao();
        serviceAccount = new SqlAccountDao();
        serviceCart = new SqlCarrelloDao();
        serviceCreate = new createOrderDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = getPath(request);
        switch(path){
            case "/show": {
                HttpSession session = request.getSession(false);
                AccountSession accountSession = getAccountSession(session);
                if(accountSession != null){
                    String email = accountSession.getEmail();
                    int page = 0;
                    try {
                        page = parsePage(request);
                    }catch (NumberFormatException e) {
                        page = 1;
                    }

                    Paginator paginator = new Paginator(page, 5);
                    int size = 0;
                    try {
                        size = serviceOrder.countAll();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        log(e.getMessage());
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                    }
                    try {
                        List<Ordine> ordini = serviceOrder.fetchOrdiniWithEmail(email, paginator);
                        request.setAttribute(LSITA_ORDINI, ordini);
                        request.getRequestDispatcher(view("account/profilo")).forward(request,response);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    response.sendRedirect("../index.html");
                }

            }
            break;
            case "/pagamento": {
                HttpSession session = request.getSession(false);
                AccountSession accountSession = getAccountSession(session);
                if(accountSession != null){
                    String email = accountSession.getEmail();
                    try {
                        Optional<Account> account = serviceAccount.fetchAccount(email);
                        if(account.isPresent()){
                            request.setAttribute(ACCOUNT, account.get());
                            request.getRequestDispatcher(view("account/pagamento")).forward(request,response);
                        }else {
                            //aggiungere messaggio alla jsp
                            request.getRequestDispatcher(view("errors/otherError")).forward(request,response);
                        }


                    } catch (SQLException e) {
                        e.printStackTrace();
                        log(e.getMessage());
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                    }
                }else {
                    request.getRequestDispatcher(view("account/pagamento")).forward(request,response);
                }


            }
            break;
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            String path = getPath(request);
            switch(path){
                case "/create": //create order(client)
                    HttpSession session = request.getSession(false);
                    if(session != null && session.getAttribute("accountSession") != null){
                        request.setAttribute("back", view("account/pagamento"));
                        validate(OrderValidator.validateForm(request));
                        AccountSession accountSession = getAccountSession(session);
                        Optional<Account> account = serviceAccount.fetchAccount(accountSession.getEmail());
                        if(account.isPresent()){
                            Carrello cart = new Carrello(serviceCart.articoliByUserEmail(accountSession.getEmail()));
                            cart.setUser(account.get());
                            cart.setEmail(accountSession.getEmail());
                            cart.getUser().setIndirizzo(request.getParameter("indirizzo"));
                            cart.getUser().setProvincia(request.getParameter("provincia"));
                            cart.getUser().setCitta(request.getParameter("citta"));
                            cart.getUser().setCap(request.getParameter("cap"));
                            cart.getUser().setTelefono(request.getParameter("telefono"));

                            cart.getUser().setCarta(request.getParameter("carta"));
                            serviceAccount.updateAccount(account.get());
                            Ordine ordineCustomer = new Ordine();
                            ordineCustomer.setCarrello(cart);
                            ordineCustomer.setTotale(cart.totale());
                            ordineCustomer.setStato("IN ELABORAZIONE");
                            ordineCustomer.setInserimento(LocalDate.now());
                            ordineCustomer.setAccount(cart.getUser());
                            synchronized (serviceCreate){
                                if(serviceCreate.createPurchase(ordineCustomer)){
                                    serviceCart.clearDBCart(accountSession.getEmail());
                                    request.getRequestDispatcher(view("customer/guestOrder")).forward(request,response);
                                }else{
                                    internalError();
                                }
                            }
                        }else {
                            notFound();
                        }
                    }else if(session != null && session.getAttribute("accountGuest") != null){
                        request.setAttribute("back", view("account/pagamento"));
                        validate(OrderValidator.validateForm(request));
                        GuestAccount accountGuest = getGuestAccount(session);
                        Ordine ordineGuest = new Ordine();
                        Account account = new Account();
                        account.setEmail(request.getParameter("email"));
                        account.setIndirizzo(request.getParameter("indirizzo"));
                        account.setProvincia(request.getParameter("provincia"));
                        account.setCitta(request.getParameter("citta"));
                        account.setCap(request.getParameter("cap"));
                        account.setTelefono(request.getParameter("telefono"));

                        account.setCarta(request.getParameter("carta"));
                        account.setAdmin(false);
                        serviceAccount.createAccountGuest(account);
                        ordineGuest.setGuessCart(accountGuest.getCart());
                        ordineGuest.setTotale(accountGuest.getCart().totale());
                        ordineGuest.setStato("IN ELABORAZIONE");
                        ordineGuest.setInserimento(LocalDate.now());
                        ordineGuest.setAccount(account);
                        synchronized (serviceCreate){
                            if(serviceCreate.createPurchase(ordineGuest)){
                                accountGuest.getCart().clearCart();
                                session.setAttribute("accountGuest",accountGuest);
                                request.getRequestDispatcher(view("customer/guestOrder")).forward(request,response);
                            }else {
                                internalError();
                            }
                        }
                    }else {
                        notAllowed();
                    }
                    break;
                case "/delete-ordine": {
                    try {
                        int id = Integer.parseInt(request.getParameter("idOrdine"));
                        if(serviceOrder.deleteOrdine(id)) {
                            List<String> msg = new ArrayList<>();
                            msg.add("Ordine annullato");
                            request.setAttribute("alert", new Alert(msg, "success"));
                        }
                        response.sendRedirect("../ordine/show");
                    } catch (SQLException e) {
                        e.printStackTrace();
                        log(e.getMessage());
                        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
        }catch (SQLException e){
            e.printStackTrace();
            log(e.getMessage());
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }catch (InvalidRequestException e){
            log(e.getMessage());
            e.handle(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
