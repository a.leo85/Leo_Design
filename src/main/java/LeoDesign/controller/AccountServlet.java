package LeoDesign.controller;

import LeoDesign.controller.http.Controller;
import LeoDesign.controller.http.InvalidRequestException;
import LeoDesign.model.account.Account;
import LeoDesign.model.account.AccountSession;
import LeoDesign.model.account.AccountValidator;
import LeoDesign.model.account.SqlAccountDao;
import LeoDesign.model.components.Alert;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Optional;


@WebServlet(name = "AccountServlet", value = "/accounts/*")
public class AccountServlet extends Controller {
    private SqlAccountDao service;

    @Override
    public void init() throws ServletException{
        super.init();
        service = new SqlAccountDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String path;
            if(request.getPathInfo() != null) path = request.getPathInfo();
            else path = "/";
            switch (path) {
                case "/secret":  //login admin (pagina)
                    request.getRequestDispatcher(view("crm/secret")).forward(request, response);
                    break;
                case "/profilo": { //pagina profilo cliente (pagina)
                    HttpSession session = request.getSession(false);
                    if (session == null) {
                        response.sendRedirect("/accounts/signin-signup");
                    } else {
                        try {
                            if(session.getAttribute("accountSession") != null){
                                AccountSession accountSession = getAccountSession(session);
                                Optional<Account> profileAccount = service.fetchAccount(accountSession.getEmail());
                                if (profileAccount.isPresent()) {
                                    request.setAttribute("profile", profileAccount.get());
                                    request.setAttribute("fullName",accountSession.getNome()+" "+accountSession.getCognome());
                                    response.sendRedirect("../ordine/show");
                                } else {
                                    notFound();
                                }
                            }
                        }catch (SQLException e) {
                            e.printStackTrace();
                            log(e.getMessage());
                            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                        } catch (InvalidRequestException e) {
                            log(e.getMessage());
                            e.handle(request, response);
                        }

                    }
                }
                break;
                case "/signin-signup": { //login o registrazione cliente (pagina)
                    HttpSession session = request.getSession(false);
                    if (session != null && session.getAttribute("accountSession") != null) {
                        AccountSession accountSession = (AccountSession) session.getAttribute("accountSession");
                        if (accountSession.isAdmin()) {
                            response.sendRedirect("../crm/dashboard");
                        } else {
                            response.sendRedirect("../ordine/show");
                        }

                    } else {
                        request.getRequestDispatcher(view("site/signin-signup")).forward(request, response);
                    }
                    break;
                }
                case "/logout": { //logout
                    HttpSession session = request.getSession(false);
                    try {
                        authenticate(session);
                        AccountSession accountSession = (AccountSession) session.getAttribute("accountSession");
                        String redirect = accountSession.isAdmin() ? "../accounts/secret" : "../index.html";
                        session.removeAttribute("accountSession");
                        session.invalidate();
                        response.sendRedirect(redirect);

                    } catch (InvalidRequestException ex){
                        log(ex.getMessage());
                        ex.handle(request,response);
                    }


                }
                break;
                default:
                    notFound();
            }
        } catch (InvalidRequestException ex){
            log(ex.getMessage());
            ex.handle(request,response);
        }



    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String path;
            if (request.getPathInfo() != null) path = request.getPathInfo();
            else path = "/";
            switch (path) {
                case "/secret": {  //login admin (ricerca nel db)
                    HttpSession session = request.getSession(false);
                    if (session != null) {
                        session.invalidate();
                    }
                    request.setAttribute("back", view("crm/secret"));
                    try {
                        validate(AccountValidator.validateSignin(request));
                    } catch (InvalidRequestException e) {
                        log(e.getMessage());
                        e.handle(request, response);
                    }
                    Account tmpAccount = new Account();
                    tmpAccount.setEmail(request.getParameter("signinEmail"));//attenzione
                    tmpAccount.setPassword(request.getParameter("signinPass"));//attenzione
                    Optional<Account> optAccount = null;

                    optAccount = service.findAccountByEmailPwd(tmpAccount.getEmail(), tmpAccount.getPassword(), true);

                    if (optAccount.isPresent()) {
                        AccountSession accountSession = new AccountSession(optAccount.get());
                        request.getSession(true).setAttribute("accountSession", accountSession);
                        response.sendRedirect("../crm/dashboard");
                    } else {
                        throw new InvalidRequestException("Credenziali non valide",
                                Arrays.asList("Credenziali non valide"), HttpServletResponse.SC_BAD_REQUEST);
                    }
                    break;
                }

                case "/signup": { //registrazione cliente
                    HttpSession session = request.getSession(false);
                    if (session != null) {
                        session.invalidate();
                    }
                    request.setAttribute("back", view("site/signin-signup"));
                    validate(AccountValidator.validateForm(request));
                    Account account = new Account();
                    account.setNome(request.getParameter("nome"));
                    account.setCognome(request.getParameter("cognome"));
                    account.setEmail(request.getParameter("signupEmail"));
                    account.setPassword(request.getParameter("signupPass"));
                    account.setAdmin(false);
                    try {
                        service.createAccount(account);
                        request.getSession(true).setAttribute("accountSession", new AccountSession(account));
                        request.setAttribute("profile", account);
                        request.setAttribute("fullName", account.getNome() + " " + account.getCognome());
                        response.sendRedirect("../ordine/show");
                    }catch (SQLException e){
                        request.setAttribute("alert", new Alert(Arrays.asList("Profilo esistente!"), "danger"));
                        request.getRequestDispatcher(view("site/signin-signup")).forward(request, response);
                    }
                    break;
                }
                case "/signin": { //login cliente (cerca nel db)
                    HttpSession session = request.getSession(false);
                    if (session != null) {
                        session.invalidate();
                    }
                    request.setAttribute("back", view("site/signin-signup"));
                    try {
                        validate(AccountValidator.validateSignin(request));
                    } catch (InvalidRequestException ex) {
                        log(ex.getMessage());
                        ex.handle(request,response);
                    }
                    Account tmpAccount = new Account();
                    tmpAccount.setEmail(request.getParameter("signinEmail"));//attenzione
                    tmpAccount.setPassword(request.getParameter("signinPass"));//attenzione
                    Optional<Account> optAccount = null;

                    optAccount = service.findAccountByEmailPwd(tmpAccount.getEmail(), tmpAccount.getPassword(), false);

                    if (optAccount.isPresent()) {
                        AccountSession accountSession = new AccountSession(optAccount.get());
                        request.getSession(true).setAttribute("accountSession", accountSession);
                        response.sendRedirect("../ordine/show");
                    } else {
                        throw new InvalidRequestException("Credenziali non valide",
                                Arrays.asList("Credenziali non valide"), HttpServletResponse.SC_BAD_REQUEST);
                    }
                    break;
                }
                default:
                   notAllowed();

            }
        } catch (SQLException e) {
            e.printStackTrace();
            log(e.getMessage());
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (InvalidRequestException ex) {
            log(ex.getMessage());
            ex.handle(request,response);
        }
    }
}
