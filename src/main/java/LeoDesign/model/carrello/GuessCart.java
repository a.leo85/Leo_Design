package LeoDesign.model.carrello;



import LeoDesign.model.prodotto.Prodotto;
import LeoDesign.model.prodotto.SqlProdottoDao;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GuessCart {
    private List<CarrelloItem> items;

    public GuessCart(){
        this.items = new ArrayList<>();
    }

    public void addItem(Prodotto prodotto, int quantity){
        for(CarrelloItem i: this.items){
            if(i.getProdotto().getIdProdotto() == prodotto.getIdProdotto()){
                i.setQuantita(i.getQuantita()+quantity);
                return;
            }
        }
        this.items.add(new CarrelloItem(prodotto, quantity));
    }
    public void updateProductsQuantity() throws SQLException {
        SqlProdottoDao service = new SqlProdottoDao();
        for(CarrelloItem i: this.items){
            i.getProdotto().setDisponibilita(service.getProductAvailability(i.getProdotto().getIdProdotto()));
        }
    }

    public void removeItem(int idProd, int quantity){
        CarrelloItem toRemove = null;
       for(CarrelloItem i: this.items){
           if(i.getProdotto().getIdProdotto() == idProd){
               if((i.getQuantita()-quantity) <= 0){
                   toRemove = i;
                   break;
               }else{
                   i.setQuantita(i.getQuantita()-quantity);
                   break;
               }
           }
       }
       if(toRemove!=null){
           this.items.remove(toRemove);
       }
    }

    public List<CarrelloItem> getItems(){
        return this.items;
    }


    public float totale() {
        float totale = 0.0F;
        for (CarrelloItem item: items){
            totale += item.totale();
        }
        return totale;
    }

    private static double roundDouble(double d, int places) {

        BigDecimal bigDecimal = new BigDecimal(Double.toString(d));
        bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
        return bigDecimal.doubleValue();
    }


    public void clearCart(){
        this.items = new ArrayList<>();
    }

}
