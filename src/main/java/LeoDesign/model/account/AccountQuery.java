package LeoDesign.model.account;

import LeoDesign.model.storage.TableQuery;

class AccountQuery extends TableQuery {
    AccountQuery(String table) {
        super(table);
    }

    String selectAccounts() {
        return String.format("SELECT * FROM %s LIMIT ?, ?;", this.table);
    }

    String selectAccount() {
        return String.format("SELECT * FROM %s WHERE email=?;", table);
    }
    String selectAccountByEmailPass() {
        return String.format("SELECT * FROM %s WHERE account_admin=? AND email=? AND pass=?;", table);
    }
    String insertAccount() {
        return String.format("INSERT INTO %s (email, pass,nome,cognome, account_admin) VALUES(?,?,?,?,?);", table);
    }
    String insertAccountGuest() {
        return String.format("INSERT INTO %s (email, indirizzo,provincia,citta,cap, telefono, carta_di_credito, account_admin) VALUES(?,?,?,?,?,?,?,?);", table);
    }
    String updateAccount() {
        return String.format("UPDATE %s SET telefono=? ,indirizzo=? ,provincia=? ,citta=? ,CAP=?, carta_di_credito=?  WHERE email=?;", table);
    }
    String deleteAccount() {
        return String.format("DELETE FROM %s WHERE email=?;", table);
    }
    String countAll() {
        return String.format("SELECT COUNT(*) FROM %s", table);
    }
}
