package LeoDesign.model.ordine;
import LeoDesign.model.categoria.SqlCategoriaDao;
import LeoDesign.model.magazzino.SqlMagazzinoDao;
import LeoDesign.model.storage.ConnManager;
import LeoDesign.model.components.Paginator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class SqlOrdineDao implements OrdineDao{
    private static final OrdineQuery QUERY = new OrdineQuery("Ordine");
    private SqlCategoriaDao servizioCategoria;
    private SqlMagazzinoDao servizioMagazzino;

    public SqlOrdineDao(){
        servizioCategoria = new SqlCategoriaDao();
        servizioMagazzino = new SqlMagazzinoDao();
    }

    @Override
    public List<Ordine> fetchOrdini(Paginator paginator) throws Exception {
        try(Connection conn = ConnManager.getConnection()){
            try(PreparedStatement ps = conn.prepareStatement(QUERY.selectOrdini())) {
                ps.setInt(1, paginator.getOffset());
                ps.setInt(2, paginator.getLimit());
                ResultSet set = ps.executeQuery();
                OrdineExtractor ordineExtractor = new OrdineExtractor();
                List<Ordine> ordini = new ArrayList<>();
                while (set.next()) {
                    ordini.add(ordineExtractor.extract(set));
                }
                return ordini;
            }
        }
    }

    public List<Ordine> fetchOrdiniWithEmail(String email, Paginator paginator) throws Exception {
        try(Connection conn = ConnManager.getConnection()){
            try(PreparedStatement ps = conn.prepareStatement(QUERY.selectOrdiniAccount())) {
                ps.setString(1,email);
                ps.setInt(2, paginator.getOffset());
                ps.setInt(3, paginator.getLimit());
                ResultSet set = ps.executeQuery();
                OrdineExtractor ordineExtractor = new OrdineExtractor();
                List<Ordine> ordini = new ArrayList<>();
                while (set.next()) {
                    ordini.add(ordineExtractor.extract(set));
                }
                return ordini;
            }
        }
    }

    @Override
    public boolean updateOrdine(String stato, int id) throws Exception {
        try(Connection conn = ConnManager.getConnection()){
            try(PreparedStatement ps = conn.prepareStatement(QUERY.updateOrdine())) {
                ps.setString(1, stato);
                ps.setInt(2, id);
                int rows = ps.executeUpdate();
                return rows == 1;

            }
        }
    }

    public boolean deleteOrdine(int idOrdine) throws Exception {
        try(Connection conn = ConnManager.getConnection()){
            try(PreparedStatement ps = conn.prepareStatement(QUERY.deleteOrdine())) {
                ps.setInt(1, idOrdine);
                int rows = ps.executeUpdate();
                return rows == 1;

            }
        }
    }

    public int countAll() throws SQLException {
        try(Connection conn = ConnManager.getConnection()){
            try(PreparedStatement stmt = conn.prepareStatement(QUERY.countAll())){
                ResultSet rs = stmt.executeQuery();
                int rows = 0;
                if(rs.next()){
                    rows = rs.getInt(1);
                }
                return rows;
            }
        }
    }
}
