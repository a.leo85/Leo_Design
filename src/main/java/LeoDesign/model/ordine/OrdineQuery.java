package LeoDesign.model.ordine;

import LeoDesign.model.storage.TableQuery;

public class OrdineQuery extends TableQuery {
    public OrdineQuery(String table) {
        super(table);
    }
    String selectOrdini() { return String.format("SELECT * FROM %s LIMIT ?, ?;", this.table); }
    String selectOrdine() { return String.format("SELECT * FROM %s WHERE idOrdine=?;", this.table); }
    String createOrdine() { return String.format("INSERT INTO %s (data_inserimento, stato_ordine, totale, cliente) VALUES(?,?,?,?);", this.table); }
    String insertCarrello() { return String.format("INSERT INTO Composizione (prodotto, ordine, quantita) VALUES(?,?,?);", this.table); }
    String insertGuestOrder() { return String.format("INSERT INTO %s (data_inserimento, stato_ordine, totale) VALUES(?,?,?);", this.table); }
    String updateOrdine() { return String.format("UPDATE %s SET stato_ordine=? WHERE idOrdine=?;", this.table); }
    String deleteOrdine() {
        return String.format("DELETE FROM %s WHERE idOrdine=?;", table);
    }
    String selectOrdiniAccount(){ return String.format("SELECT * FROM %s WHERE cliente=? LIMIT ?, ?", this.table); }
    String selectOrdiniWithProdotti(){ return String.format("", this.table); }
    String countAll() {
        return String.format("SELECT COUNT(*) FROM %s", table);
    }
}
