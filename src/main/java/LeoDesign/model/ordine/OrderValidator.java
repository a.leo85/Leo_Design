package LeoDesign.model.ordine;

import LeoDesign.controller.http.RequestValidator;
import jakarta.servlet.http.HttpServletRequest;

import java.util.regex.Pattern;

public class OrderValidator {

    public static RequestValidator validateForm(HttpServletRequest request) {
        RequestValidator validator = new RequestValidator(request);
        validator.assertEmail("email", "Email non valida");
        validator.assertMatch("indirizzo", Pattern.compile("^[a-zA-Z0-9-'/& ]{5,}$"), "L'indirizzo deve contenere anche delle lettere");
        validator.assertMatch("provincia", Pattern.compile("^[^0-9]{1,}$"), "Provincia non valida");
        validator.assertMatch("citta", Pattern.compile("^[^0-9]{1,}$"), "La citta non deve contenere numeri");
        validator.assertMatch("cap", Pattern.compile("^[0-9]{1,5}$"), "CAP non supportato");
        validator.assertMatch("telefono", Pattern.compile("^[0-9]{10}$"), "Numero di telefono non valido");
        validator.assertMatch("carta", Pattern.compile("^[0-9]{16}$"), "Carta non valida");
        return validator;
    }
}
