package LeoDesign.model.ordine;

import LeoDesign.model.components.Paginator;

import java.util.List;
import java.util.Optional;

public interface OrdineDao<E extends Exception>{
    List<Ordine> fetchOrdini(Paginator paginator) throws E;
    boolean updateOrdine(String stato, int id) throws E;
}
