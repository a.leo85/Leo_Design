package LeoDesign.model.magazzino;

import LeoDesign.model.components.Paginator;

import java.sql.SQLException;
import java.util.List;

public interface MagazzinoDao {
    Magazzino fetchMagazzino(int id) throws SQLException;

}
