package LeoDesign.model.magazzino;

public class Magazzino {
    private int idMagazzino;
    private String nomeMagazzino;

    public Magazzino() {
        super();
    }

    public int getIdMagazzino() {
        return idMagazzino;
    }

    public void setIdMagazzino(int idMagazzino) {
        this.idMagazzino = idMagazzino;
    }

    public String getNomeMagazzino() {
        return nomeMagazzino;
    }

    public void setNomeMagazzino(String nomeMagazzino) {
        this.nomeMagazzino = nomeMagazzino;
    }
}
