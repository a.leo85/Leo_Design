package LeoDesign.model.magazzino;

import LeoDesign.model.storage.ConnManager;
import LeoDesign.model.components.Paginator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SqlMagazzinoDao implements MagazzinoDao {
    private static final MagazzinoQuery QUERY = new MagazzinoQuery("Magazzino");
    @Override
    public Magazzino fetchMagazzino(int id) throws SQLException {
        try(Connection conn = ConnManager.getConnection()){
            try(PreparedStatement ps = conn.prepareStatement(QUERY.selectMagazzino())) {
                ps.setInt(1, id);
                ResultSet set = ps.executeQuery();
                MagazzinoExtractor magazzinoExtractor = new MagazzinoExtractor();
                Magazzino magazzino = null;
                if (set.next()) {
                    magazzino = magazzinoExtractor.extract(set);
                }
                return magazzino;
            }
        }
    }
}
