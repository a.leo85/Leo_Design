package LeoDesign.model.categoria;

import java.sql.SQLException;
import java.util.List;

public interface CategoriaDao {
    Categoria fetchCategoriaById(int id) throws SQLException;
    Categoria fetchCategoriaWithProdotti(int categoriaId) throws SQLException;
}
