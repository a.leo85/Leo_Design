package LeoDesign.model.prodotto;

import LeoDesign.controller.http.RequestValidator;
import jakarta.servlet.http.HttpServletRequest;

import java.util.regex.Pattern;

public class ProductValidator {

    public static RequestValidator validateForm(HttpServletRequest request) {
        RequestValidator validator = new RequestValidator(request);
        validator.assertMatch("nome", Pattern.compile("^[a-zA-Z0-9-'/& ]{4,60}$"), "Nome compreso tra i 4 e 60 caratteri");
        validator.assertMatch("descrizione", Pattern.compile("^.{20,200}$"), "Descrizione compresa tra i 20 e 200 caratteri");
        validator.assertDouble("prezzo", "Prezzo deve essere un numero con la virgola");
        validator.assertMatch("peso", Pattern.compile("^\\d+(\\.\\d+)?$"), "Peso deve essere un numero");
        validator.assertInt("disponibilita","Disponibilita deve essere un intero");
        validator.assertInt("magazzino","Valore magazzino errato");
        validator.assertInt("categoria","Valore categoria errato");
        validator.assertMatch("immagine1", Pattern.compile("^(?i:^.*\\.(jpg)$)$"), "Estensione immagine1 errato");
        validator.assertMatch("immagine2", Pattern.compile("^(?i:^.*\\.(jpg)$)$"), "Estensione immagine2 errato");
        validator.assertMatch("immagine3", Pattern.compile("^(?i:^.*\\.(jpg)$)$"), "Estensione immagine3 errato");
        return validator;
    }
}
